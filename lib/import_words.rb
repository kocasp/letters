class ImportWords

  # gets a name of the file from db folder that cotains words in separate lines to import
  def self.load(filename)
    p "importing word list from ile to database..."
    file_path = Rails.root.join('db', filename).to_s
    command = "COPY words(Word) FROM '#{file_path}'  DELIMITER AS ';';"
    ActiveRecord::Base.connection.execute(command)
  end
end
