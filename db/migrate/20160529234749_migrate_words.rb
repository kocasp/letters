class MigrateWords < ActiveRecord::Migration[5.0]
  def up
    ImportWords.load("words_eng.txt")
  end

  def down
    Word.delete_all
  end
end
