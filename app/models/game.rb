# This is the core engine of actioncable. Broadcast methods can also be placed
# in channel file instead
class Game
  def self.start(uuid1, uuid2)
    white, black = [uuid1, uuid2].shuffle

    REDIS.set("opponent_for:#{white}", black)
    REDIS.set("opponent_for:#{black}", white)

    ActionCable.server.broadcast "player_#{uuid1}", {action: "game_start", msg: "white"}
    ActionCable.server.broadcast "player_#{uuid2}", {action: "game_start", msg: "black"}
    random_letter = (65 + rand(26)).chr
    ActionCable.server.broadcast "player_#{uuid1}", {action: "add_letter", msg: {letter: random_letter, side: "right"}}
    ActionCable.server.broadcast "player_#{uuid2}", {action: "add_letter", msg: {letter: random_letter, side: "right"}}
    ActionCable.server.broadcast "player_#{uuid1}", {action: "set_turn"}
  end

  def self.forfeit(uuid)
    if winner = opponent_for(uuid)
      ActionCable.server.broadcast "player_#{winner}", {action: "opponent_forfeits"}
    end
  end

  def self.opponent_for(uuid)
    REDIS.get("opponent_for:#{uuid}")
  end

  def self.make_move(uuid, data)
    opponent = opponent_for(uuid)
    move_string = "#{data["from"]}-#{data["to"]}"

    ActionCable.server.broadcast "player_#{opponent}", {action: "make_move", msg: move_string}
  end

  def self.add_letter(uuid, data)
    opponent = opponent_for(uuid)
    ActionCable.server.broadcast "player_#{opponent}", {action: "add_letter", msg: data}
    ActionCable.server.broadcast "player_#{uuid}", {action: "add_letter", msg: data}
    if word_finished(data)
      ActionCable.server.broadcast "player_#{opponent}", {action: "won", msg: "Oponnent finished the word"}
      ActionCable.server.broadcast "player_#{uuid}", {action: "lost", msg: "You finished the word"}
    else
      ActionCable.server.broadcast "player_#{opponent}", {action: "set_turn"}
    end
  end

  def self.check_opponent(uuid, data)
    opponent = opponent_for(uuid)
    check_word(opponent, {message: "Oponnent checks!"})
  end

  def self.check_word(uuid, data)
    ActionCable.server.broadcast "player_#{uuid}", {action: "check_word", msg: data}
  end

  def self.answer_check(uuid, data)
    opponent = opponent_for(uuid)
    if !data['answer'].downcase.include? data['word'].downcase
      check_word(uuid, {message: "The answer doesnt contain current letters!"})
    elsif !Word.find_by_word(data['answer'].downcase).present?
      ActionCable.server.broadcast "player_#{uuid}", {action: "lost", msg: "word not found in dictionary"}
      ActionCable.server.broadcast "player_#{opponent}", {action: "won", msg: "oponnent did not provide correct word"}
    else
      ActionCable.server.broadcast "player_#{uuid}", {action: "won", msg: "word correct"}
      ActionCable.server.broadcast "player_#{opponent}", {action: "lost", msg: "oponnent provided correct word: #{data['answer'].downcase}"}
    end
  end

  def self.word_finished(data)
    if data['side'] == "right"
      word = "#{data['word']}#{data['letter']}"
    elsif data['side'] == "left"
      word = "#{data['letter']}#{data['word']}"
    end
    word = word.downcase

    word.length > 3 && Word.find_by_word(word).present?
  end
end
