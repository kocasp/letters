class Seek
  def self.create(room_name, uuid)
    if opponent = REDIS.spop(room_name)
      Game.start(uuid, opponent)
    else
      REDIS.sadd(room_name, uuid)
    end
  end

  def self.remove(room_name, uuid)
    REDIS.srem(room_name, uuid)
  end

  def self.clear_all(room_name)
    REDIS.del(room_name)
  end
end
