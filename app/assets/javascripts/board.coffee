# this is an actioncable model which sends data to rails through channel
# this file is loaded automatically and his functions are stored in App class

$(".rooms.show").ready ->
  if $(".rooms.show").length > 0

    addLetter = (side, letter, word) ->
      App.game.perform("add_letter", {
        action: "add_letter",
        side: side,
        letter: letter,
        word: word
        })
    App.addLetter = addLetter

    checkOpponent = (word) ->
      console.log(word)
      App.game.perform("check_opponent", {
        action: "check_opponent",
        word: word
        })
    App.checkOpponent = checkOpponent

    showControles = () ->
      $("#controles").show()
      $("#opponent-move").hide()
    App.showControles = showControles

    hideControles = () ->
      $("#controles").hide()
      $("#opponent-move").show()
    App.hideControles = hideControles

    hideFriendLink = () ->
      $("#friend-link").hide()
    App.hideFriendLink = hideFriendLink

    showYouWin = (msg) ->
      if $('.showSweetAlert:contains("You")').length == 0
        sweetAlert(
          {
            title: 'You won! ',
            text: msg,
            type: 'success'
          },
          () ->
            location.reload()
        );
    App.showYouWin = showYouWin

    showYouLoose = (msg) ->
      sweetAlert(
        {
          title: 'You lost! ',
          text: msg,
          type: 'error'
        },
        () ->
          location.reload()
      );
    App.showYouLoose = showYouLoose

    askForWord = (message) ->
      swal {
        title: 'Oponnent cheks!'
        text: message
        type: 'input'
        showCancelButton: true
        closeOnConfirm: false
        animation: 'slide-from-top'
        inputPlaceholder: 'Write something'
        showCancelButton: false
      }, (answer) ->
        if answer == false
          return false
        if answer == ''
          swal.showInputError 'You need to write something!'
          return false
        word = $(".game_word").first().text()
        App.answerCheck(word, answer)
        return
    App.askForWord = askForWord

    answerCheck = (word, answer) ->
      App.game.perform("answer_check",{
        action: "answer_check",
        word: word,
        answer: answer
        })
    App.answerCheck = answerCheck

