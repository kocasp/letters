# This is an ActionCable -> rails backend linkage
# Be sure to restart your server when you modify this file. Action Cable runs in an EventMachine loop that does not support auto reloading.
class GameChannel < ApplicationCable::Channel
  def subscribed
    room_name = params[:room_name]
    stream_from "player_#{uuid}"
    Seek.create(room_name, uuid)
  end

  def unsubscribed
    room_name = params[:room_name]
    Seek.remove(room_name, uuid)
    Game.forfeit(uuid)
  end

  def make_move(data)
    Game.make_move(uuid, data)
  end

  def add_letter(data)
    Game.add_letter(uuid, data)
  end

  def check_opponent(data)
    Game.check_opponent(uuid, data)
  end

  def answer_check(data)
    Game.answer_check(uuid, data)
  end
end
