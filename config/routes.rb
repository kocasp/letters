Rails.application.routes.draw do
  mount ActionCable.server => "/cable"
  
  root to: "rooms#index"
  resources :welcome, only: [:index]
  resources :rooms, only: [:index, :show]
end
